var material = {
  "Camiseta":4.5,
  "Camiseta Manga Longa":4,
  "Jaleco":1,
  "Calça":1,
  "Short":6
}

var tecido = {
  "Algodão": 38.90,
  "Poliviscose":29.90,
  "100% Poliester":22.90,
  "Piquet":39.99,
  "Dry Fit":39.90,
  "Acqua Poliamida":79.90,
  "Porus Dry":79.90
}

var acabamento = {
  "Gola Polo": 38.90,
  "Gola Ribana":29.90,
  "Viés":22.90,
  "Ziper":39.99,
  "Botão":39.99,
}

var personalizacao = {
  "Silk":{
    "Plastisol":{
      "Total":11,
      "Frente":12,
      "Costa":13,
      "Manga":14,
    },
    "Hydrocril":{
      "Total":21,
      "Frente":22,
      "Costa":23,
      "Manga":24,
    },
    "Glitter":{
      "Total":31,
      "Frente":32,
      "Costa":33,
      "Manga":34,
    },
    "Puff":{
      "Total":41,
      "Frente":42,
      "Costa":43,
      "Manga":44,
    }
  },

  "Bordado":{
    "Peito":1,
    "Frente":2,
    "Costa":3,
    "Manga":4,
  }
};

var tipoPersonalizacao;
var descSilk;
var localSilk;
var arraySilk;
var arrayBordado;
var localBordado;
var valorSilk;
var valorBordado;

$('.desc-silk').hide();
$('.local-silk').hide();
$('.qtd-cores').hide();

// var numeroClasse = $('.form-personalizacao')[0];
//   numeroClasse.value = numeroClasse.value.replace(/.$/, '');

for(prop in personalizacao){
  $(".personalizacao").append("<option>" + prop + "</option>");   
}

$('.personalizacao').change(function(){
  tipoPersonalizacao = $(this).val();
  console.log(tipoPersonalizacao);

  if(tipoPersonalizacao == 'Silk'){
    $('.desc-silk').children('option:not(:first)').remove();
    for(prop in personalizacao.Silk){
      $(".desc-silk").append("<option>" + prop + "</option>");   
    }
    $('.desc-silk').show();

    //Apresentar o local da personalização em silk
    $('.desc-silk').change(function(){
      $('.local-silk').children('option:not(:first)').remove();
      descSilk = $(this).val();
      switch(descSilk){
        case 'Plastisol':
          for(prop in personalizacao.Silk.Plastisol){
            $(".local-silk").append("<option>" + prop + "</option>");   
          }
          localSilk = personalizacao.Silk.Plastisol
          break;
        case 'Hydrocril':
          for(prop in personalizacao.Silk.Hydrocril){
            $(".local-silk").append("<option>" + prop + "</option>");   
          }
          localSilk = personalizacao.Silk.Hydrocril
          break
        case 'Glitter':
          for(prop in personalizacao.Silk.Glitter){
            $(".local-silk").append("<option>" + prop + "</option>");   
          }
          localSilk = personalizacao.Silk.Glitter
          break
        case 'Puff':
          for(prop in personalizacao.Silk.Puff){
            $(".local-silk").append("<option>" + prop + "</option>");   
          }
          localSilk = personalizacao.Silk.Puff
          break
      }
      $('.local-silk').show();
      console.log(descSilk);

      //Função para apresentar os valores de cada impressao
      $('.local-silk').change(function(){
        arraySilk = $(this).val();
        switch(arraySilk){
          case 'Total':
              valorSilk = localSilk[arraySilk]
            break;
          case 'Frente':
              valorSilk = localSilk[arraySilk]
            break;
          case 'Costa':
              valorSilk = localSilk[arraySilk]
            break;
          case 'Manga':
              valorSilk = localSilk[arraySilk]
            break;
        }
        $('.qtd-cores').show();
        console.log(valorSilk)
      });
    });    
  }

  //Apresentar os valores de cada bordado
  if(tipoPersonalizacao === 'Bordado'){
    $('.qtd-cores').hide();
    $('.desc-silk').hide();
    $('.local-silk').show();
    $('.local-silk').children('option:not(:first)').remove();
    for(prop in personalizacao.Bordado){
      $(".local-silk").append("<option>" + prop + "</option>");   
    }
    localBordado = personalizacao.Bordado;
    $('.local-silk').change(function(){
      arrayBordado = $(this).val();
        switch(arrayBordado){
          case 'Peito':
              valorBordado = localBordado[arrayBordado]
            break;
          case 'Frente':
              valorBordado = localBordado[arrayBordado]
            break;
          case 'Costa':
              valorBordado = localBordado[arrayBordado]
            break;
          case 'Manga':
              valorBordado = localBordado[arrayBordado]
            break;
        }
        console.log(valorBordado)
    });
  }
});


//Acao de click para vria um novo campo de impressao
var max_campos = 4; 
var x = 1;
var cloneCount = 1;
$(document).on('click','#bt-add-imp',function(e){
  e.preventDefault(); //prevenir novos clicks
  if (x < max_campos) {
    $('.form-personalizacao:last').clone(true).attr('class', 'form-personalizacao' + cloneCount++).insertAfter(".form-personalizacao:last");
    x++;
  }
})

$(document).on('click','#bt-remove-imp',function(e){
  e.preventDefault();
  $('.form-personalizacao:last').remove();
  x--;
});


////////////////////
for(prop  in material){
  $("#material").append("<option>" + prop + "</option>");
}

for(prop in tecido){
   $("#tecido").append("<option>" + prop + "</option>");
}

for(prop in acabamento){
   $("#acabamento").append("<input type='checkbox' value=" + acabamento[prop] + "><label>" + prop + "</label>");
}











  
$('#bt-calcular').click(function(){
  quantidade = $('#qtd-pecas').val();
  total = quantidade / material[resultadoMaterial];
  console.log(total)
})


