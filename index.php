<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<form>
		<fieldset>
		<legend>Orçamentos</legend>
			
			<fieldset>
				<legend>Quantidade/Material</legend>
				<label>Quantidade:</label>
				<input id="input-number" type="text" name="quantidade" placeholder="" maxlength="4">
				
				<label>Material:</label>
				<select id="estado">
				  <option value="camiseta">Camiseta</option>
				  <option value="camisaLonga">Camisa manga longa</option>
				  <option value="jaleco">Jaleco</option>
				  <option value="calca">Calça</option>
				  <option value="short">Short</option>
				</select>

				<label>Tecido:</label>
				<select id="tecido">
				  <option class="option-camiseta" value="algodao">Algodão</option>
				  <option class="option-camiseta" value="poliviscose">Poliviscose</option>
				  <option class="option-camiseta" value="poliester">100% Poliester</option>
				  <option class="option-camiseta" value="dryfit">Dry Fit</option>
				</select>
			</fieldset>
			
			<fieldset>
				<legend>Acabamento</legend>
				<input type="checkbox"><label>Gola Polo</label>
				<input type="checkbox"><label>Gola Ribana</label>
				<input type="checkbox"><label>Punho Polo</label>
				<input type="checkbox"><label>Punho Ribana</label>
				<input type="checkbox"><label>Viés</label>
				<input type="checkbox"><label>Ziper</label>
				<input type="checkbox"><label>Normal</label>
			</fieldset>
			
			<fieldset>
			<legend>Impressão/Bordado</legend>
				<select>
					<option value="serigrafia">Serigrafia</option>
					<option value="sublimacao">Sublimação</option>
					<option value="transfer">Transfer</option>
					<option value="foil">Foil</option>
					<option value="foil">Bordado</option>
				<select>

				<select>
					<option value="hidrocryl">Normal</option>
					<option value="puff">Puff</option>
					<option value="glitter">Glitter</option>
					<option value="plastisol">Plastisol</option>
				<select>
				
				<label>Quantidade de cores:</label>
				<input id="input-number" type="text" name="quantidade" placeholder="" maxlength="2">

				<label>Impressão 100%:</label>
				<input type="radio"><label>Sim</label>
				<input type="radio"><label>Não</label>
			</fieldset>

			<fieldset>
			<legend>Valores</legend>
			<label>Valor Total Final:</label><input id="input-number" type="text" disabled>
		</fieldset>
	</form>
	<script src="calculo-camiseta.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
	
</body>
</html>